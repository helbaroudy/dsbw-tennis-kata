

trait TennisPrinter {
  val possibleScore = Array("0", "15", "30", "40", "ADV", "WIN")
  def printScore(score: ScoreBoard): Unit
}

class TennisSet(player1: String, player2: String, val printer: TennisPrinter) {
  val local = new TennisPlayer(player1)
  val visitant = new TennisPlayer(player2)
  var scoreBoard = new ScoreBoard(local, visitant)

  def getPlayerNames: (String,String) = {
    (local.name, visitant.name)
  }

  def addPoint(name: String) = {
    scoreBoard.addPoint(name)
    printer.printScore(scoreBoard)
  }

  def getResultString: String = {
    scoreBoard.getResultString
  }

  def getWinner: Option[TennisPlayerName] = {
    scoreBoard.winner.map(_.getTennisPlayerName)
  }
}

class Player(val name: String) {}

// Aplicant patró DTO hem creat una classe DTO separada per no retornar TennisPlayer al client (Test)
// Abans de conèixer DTO l'opció haugés estat segregar interfícies... però és important triar bé el nom de la intefície
// (tenies un petit embolic de noms)
case class TennisPlayerName(name:String)


// No encapsula realment score, per tant, potser no val la pena
// Per patró Expert seria interessant que encapsulés l'accés als seus atributs, sobretot quan és accés de modificació
// Si no el pot encapsular, potser no val la pena aquesta classe
class TennisPlayer(val name: String, var score: Int = 0) {

  def addPoint() = {
    score += 1
  }

  def getTennisPlayerName:TennisPlayerName = TennisPlayerName(name)
}

class ScoreBoard(val local: TennisPlayer, val visitor: TennisPlayer) {
  var winner: Option[TennisPlayer] = None
  def addPoint(name: String) = {
    if (winner == None) {
      name match {
        case local.name =>
          local.addPoint()
        case visitor.name =>
          visitor.addPoint()
        case _ => throw new Exception(name + " doesn't exists")
      }
      processScore()
    } else{
      throw new RuntimeException("Ended Set")
    }
  }

  def setWinner(player: TennisPlayer): Unit = {
    winner = Some(player)
    player.score = 5
  }

  def removeDeuce(player1: TennisPlayer): Unit = player1.score -= 1

  def processScore(): Unit = {
    println(local.score + "," + visitor.score)
    val difference = getDifferenceLocalVisitor

    //CASO "BASE"
    if (visitor.score >= 3 && local.score < 3 && Math.abs(difference) >= 2) setWinner(visitor)
    if (local.score >= 3 && visitor.score < 3 && Math.abs(difference) >= 2) setWinner(local)

    //Casos no senzillos
    if (visitor.score >= 3 && local.score >= 3) {
      if (difference >= 2) {
        setWinner(local)
      }
      else if (difference <= -2) {
        setWinner(visitor)
      }
      if (visitor.score > 3 && local.score > 3) {
        setDeuce()
      }
    }
  }


  def setDeuce(): Unit = {
    visitor.score = 3
    local.score = 3
  }

  def getDifferenceLocalVisitor: Int = local.score - visitor.score

  def getResultString: String =
    "(" + local.name + " " + local.score + ";" + visitor.name + " " + visitor.score + ")"

}

//Logica
/*
  0 - 0
  1 - 15
  2 - 30
  3 - 40
  4 - (if x== 3) deuce
  5 - ADV (if == 4 -> ++1)
  6 - WIN
*/
