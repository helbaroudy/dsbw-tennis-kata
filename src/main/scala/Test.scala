

object Test extends App {

  //IMPLEMENTA EL TRAT
  class MyPrinter extends TennisPrinter {
    override def printScore(score: ScoreBoard): Unit = {
      println("Current result ("
        + score.local.name + ":" + possibleScore.apply(score.local.score) + " " + score.local.score + " ; "
        + score.visitor.name + ":" + possibleScore.apply(score.visitor.score) + " " + score.visitor.score + ")")

      if (score.winner != None) println("Set ended Winner:" + score.winner.get.name)
    }

    // override def printWinner(player: Some[TennisPlayer]): Unit = println("Winner: " + player.get.name)
  }

  def set2(): Unit = {
    val set = new TennisSet("jugadorA", "jugadorB", new MyPrinter())
    println(set.getPlayerNames)

    set.addPoint("jugadorA")
    set.addPoint("jugadorA")
    set.addPoint("jugadorA")
    set.addPoint("jugadorA")
  }

  def set1(): Unit = {
    val set = new TennisSet("jugadorA", "jugadorB", new MyPrinter())
    // set.getPlayerNames
    // set.getResultString
    // set.getWinner

    println(set.getPlayerNames)

    set.addPoint("jugadorA")
    set.addPoint("jugadorA")
    set.addPoint("jugadorB")

    println(set.getWinner)

    set.addPoint("jugadorB")
    println(set.getWinner)

    set.addPoint("jugadorB")
    //    println(set.getWinner.get.name)

    set.addPoint("jugadorA")
    set.addPoint("jugadorB")
    set.addPoint("jugadorA")

    set.addPoint("jugadorB")
    val w = set.getWinner.get

    //println(set.getWinner.get.name)
    set.addPoint("jugadorB")
    set.addPoint("jugadorB")
    set.addPoint("pepe")

  }

  set1()
  set2()


  //set.isFinished();

}
